module Floating(represention,derepresention,harkin) where

import Utils.Compose
import Numeric
import Bools
import Data.Bits

type FloatSpec = (Int,Int) -- (a,b) => 1 Sign + a Exponent + b Mantissa
type Floaty = Bools

represention :: FloatSpec -> String -> String
represention = showbools .#. represent

derepresention :: FloatSpec -> String -> String
derepresention sp = unrepresent sp . represent sp


unrepresent :: FloatSpec -> Floaty -> String
unrepresent (e,m) (Bools b) = sign ++ number
                    where 
                        sign = if b !! 0 then "-" else ""
                        (exponent,mantissa) = (take e . drop 1 $ b, take m . drop (e+1) $ b)
                        number = if intify exponent == negate (2^(e-1))
                                    then "0"
                                    else harkin ('1':fmap(doelif '1' '0')mantissa) (intify exponent+1)

harkin :: String -> Int -> String
harkin number offset | offset >= length number  = number ++ replicate (offset - length number) '0'
                     | offset >= 0              = insertAt '.' offset number
                     | otherwise                = "0." ++ replicate (negate offset) '0' ++ number

insertAt :: a -> Int -> [a] -> [a]
insertAt c n x = take n x ++ [c] ++ drop n x

represent :: FloatSpec -> String -> Floaty
represent s x | not ('-' `elem` take 1 x) = False .:. represent' s (parseBinary x)
              | otherwise                 =  True .:. represent' s (parseBinary $ drop 1 x)

represent' :: FloatSpec -> (Int,Bool,Bools) -> Floaty
represent' (e,m) (_,False,_) = Bools $ replicate 1 True ++ replicate (e+m-1) False
represent' (e,m) (x,True ,b) = encode e x .++. takebools m b

encode :: Int -> Int -> Bools
encode e x | x >= 0     = lastbools e              . encode' e $ x
           | otherwise  = lastbools e . complement . encode' e $ negate (x+1)

encode' :: Int -> Int -> Bools
encode' n x = Bools $ replicate n False ++ (fmap (=='1') $ showIntAtBase 2 (head . show) x "")

parseBinary :: String -> (Int,Bool,Bools)
parseBinary s = (radixoffset,nonzero,mantissa)
        where
            nonzero = '1' `elem` s

            radixloc' = filter (('.'==) . snd) . zip [0..] $ s
            radixloc = if length radixloc' == 0 then length s else fst . head $ radixloc'
            oneloc' = filter (('1'==) . snd) . zip [0..] $ s
            oneloc = if length oneloc' == 0 then length s else fst . head $ oneloc'

            radixoffset = if not nonzero then 0 else ifnegsucc $ radixloc - oneloc - 1
            mantissa = if not nonzero
                        then zeroBits 
                        else Bools 
                            . fmap (=='1') 
                            . filter (\c -> c == '0' || c == '1') 
                            . drop (oneloc+1) 
                            $ s

ifnegsucc :: Int -> Int
ifnegsucc x = if x < 0 then x + 1 else x
