module Bools where


import Utils.Compose
import Data.Bits

newtype Bools = Bools {getbools::[Bool]} deriving(Eq,Show)

instance Bits a => Bits [a] where
    (.&.) = zipWith (.&.)
    (.|.) = zipWith (.|.)
    xor = zipWith xor
    complement = fmap complement
    shift = flip $ fmap . flip shift
    rotate = flip $ fmap . flip rotate
    bitSize = sum . fmap bitSize
    bitSizeMaybe = fmap sum . sequence . fmap bitSizeMaybe
    isSigned = any isSigned
    --testBit = undefined
    --bit n = undefined
    popCount = sum . fmap popCount

zipWithgetbools :: (Bool -> Bool -> Bool) -> Bools -> Bools -> Bools
zipWithgetbools f a b = Bools $ zipWith f (getbools a) (getbools b)

instance Bits Bools where
    (.&.) = zipWithgetbools (.&.)
    (.|.) = zipWithgetbools (.|.)
    xor = zipWithgetbools xor
    complement = Bools . fmap complement . getbools
    shift = Bools .#. flip (fmap . flip shift) . getbools
    rotate = Bools .#. flip (fmap . flip rotate) . getbools
    bitSize = length . getbools
    bitSizeMaybe = Just . length . getbools
    isSigned = const False
    testBit = (== True) .#. (!!) . getbools
    bit n = Bools $ replicate (n-1) False ++ [True] ++ repeat False
    popCount = sum . fmap popCount . getbools


(.++.) :: Bools -> Bools -> Bools
(Bools a) .++. (Bools b) = Bools $ a ++ b

(.:.) :: Bool -> Bools -> Bools
a .:. (Bools b) = Bools $ a : b

takebools :: Int -> Bools -> Bools
takebools n = Bools . take n . getbools
lastbools :: Int -> Bools -> Bools
lastbools n = Bools . takeLast n . getbools
dropbools :: Int -> Bools -> Bools
dropbools n = Bools . drop n . getbools

takeLast :: Int -> [a] -> [a]
takeLast n x = drop (length x - n) x


uintifybools :: Bools -> Int
uintifybools = uintify . getbools
intifybools :: Bools -> Int
intifybools = intify . getbools

uintify :: [Bool] -> Int
uintify = foldl (\n b -> 2*n + doelif 1 0 b) 0
intify :: [Bool] -> Int
intify x = doelif (flip (-) (2^length x)) id (x !! 0) $ uintify x


doelif :: a -> a -> Bool -> a
doelif a _ True  = a
doelif _ a False = a

showbools :: Bools -> String
showbools = insertEvery 4 " " . fmap (doelif '1' '0') . getbools

insertEvery :: Int -> [a] -> [a] -> [a]
insertEvery n [] a = a
insertEvery n _ [] = []
insertEvery n x a  = take n a ++ x ++ insertEvery n x (drop n a)
