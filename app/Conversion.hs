module Conversion where

import Prelude hiding(splitAt)

dec2bin :: Int -> String -> String
dec2bin prec s = undefined --sign ++ number
    where
        (sign,rest) = if s !! 0 == '-' then ("-",drop 1 s) else ("",s)
        (pre,post) = splitAt '.' rest
        intpart = foldl (\n c -> 2*n + c2i c) 0 pre
        (fracpart,len) = (foldl (\n c -> 2*n + c2i c) 0 post, length post)

splitAt :: Eq a => a -> [a] -> ([a],[a])
splitAt x a = (takeWhile (/=x) a, drop 1 $ dropWhile (/=x) a)

c2i :: Char -> Integer
c2i = read . return
