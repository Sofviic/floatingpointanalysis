module Utils.Tuple
    (map4a,sequence4,
    fst4,snd4,trd4,fth4
    ) where

map4a :: (a -> b) -> (a,a,a,a) -> (b,b,b,b)
map4a f (a,b,c,d) = (f a,f b,f c,f d)

sequence4 :: [(a,b,c,d)] -> ([a],[b],[c],[d])
sequence4 x = (fmap fst4 x, fmap snd4 x, fmap trd4 x, fmap fth4 x)

fst4 :: (a,b,c,d) -> a
fst4 (x,_,_,_) = x
snd4 :: (a,b,c,d) -> b
snd4 (_,x,_,_) = x
trd4 :: (a,b,c,d) -> c
trd4 (_,_,x,_) = x
fth4 :: (a,b,c,d) -> d
fth4 (_,_,_,x) = x
