module Utils.Compose((.#.),(.##.)) where

infixr 9 .#.
(.#.) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
f .#. g = (f .) . g

infixr 9 .##.
(.##.) :: (c -> d) -> (a -> b0 -> b1 -> c) -> a -> b0 -> b1 -> d
f .##. g = ((f .) .) . g

{-
infixr 8 .#.
infixr 8 .##.
infixr 8 .###.
infixr 8 .####.
infixr 8 .#####.
(.#.) = (.)
(.##.) f = ((f .) .)
(.###.) f = (((f .) .) .)
(.####.) f = ((((f .) .) .) .)
(.#####.) f = (((((f .) .) .) .) .)
-}