module Utils.Arithmetic(average,clamp) where

average :: Integral a => [a] -> a
average x = sum x `div` (fromIntegral.length) x

clamp :: Ord a => a -> a -> a -> a
clamp a b = max a . min b
