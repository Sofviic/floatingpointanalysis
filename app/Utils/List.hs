module Utils.List(fmaps) where

fmaps :: [a -> b] -> a -> [b]
fmaps fs = flip fmap fs . flip ($)
